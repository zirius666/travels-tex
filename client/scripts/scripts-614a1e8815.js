"use strict";

/**
 * @ngdoc overview
 * @name travelsApp
 * @description
 * # travelsApp
 *
 * Main module of the application.
 */
angular
    .module("travelsApp", [
        "ngAnimate",
        "ngCookies",
        "ngMessages",
        "ngResource",
        "ngRoute",
        "ngSanitize",
        "ngTouch",
        "lbServices",
        "ui.bootstrap",
        "ui.router",
        "toaster",
        'ui.grid',
        'angularMoment',
        "oitozero.ngSweetAlert",
        'ngFileUpload'
    ])
    .config(["$locationProvider", function($locationProvider) {
        $locationProvider.hashPrefix("");
    }])
    .run(function($rootScope, oauth, currencyConversion, currentUser) {

        $rootScope.logged = oauth.isLoggedIn ? true : false;
        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams) {
                // do something
                console.log("Get currencies...");
                if ($rootScope.currencies == undefined) {
                    $rootScope.currencies =[];
                    currencyConversion.getCurrencies("MXN", 1)
                        .then((res) => {
                            console.log(res);
                            angular.forEach(res.rates,(value, key)=>{
                                $rootScope.currencies.push(key);

                            });
                            $rootScope.dataRates = res;
                            $rootScope.currencies.push("MXN");
                            console.log(currentUser.currency.selectedCurrency);
                          if(currentUser.currency.selectedCurrency != undefined){
                            $rootScope.selectedCurrency = currentUser.currency.selectedCurrency;
                          }else{
                            $rootScope.selectedCurrency = 'MXN';
                          }

                            console.log($rootScope.currencies)
                        })
                        .catch((err) => {
                            console.log(err);
                        })
                }

            })
    })
    .config(["$stateProvider", "$urlRouterProvider", "$sceProvider",
        function($stateProvider, $urlRouterProvider, $sceProvider) {
            $sceProvider.enabled(false);
            $urlRouterProvider.otherwise("/");

            $stateProvider
                .state("main", {
                    url: "/",
                    templateUrl: "/views/main.html",
                    controller: "MainCtrl",
                    controllerAs: "main"
                })
                .state("dashboard", {
                    url: "/dashboard",
                    templateUrl: "/views/dashboard.html",
                    controller: "DashboardCtrl",
                    controllerAs: "dashboard"
                })
                .state("travels", {
                    url: "/travels/:travelID",
                    templateUrl: "views/travels.html",
                    controller: "TravelsCtrl",
                    controllerAs: "travels"
                })
                .state("expenses", {
                    url: "/expenses/:expenseID/:travelID",
                    templateUrl: "views/expenses.html",
                    controller: "ExpensesCtrl",
                    controllerAs: "expenses"
                });
            /* .otherwise({
             redirectTo: "/"
             });*/
        }
    ]);

'use strict';


var APIURL = 'https://nameless-river-54607.herokuapp.com/api';
var ISINPRODUCTION = true;

angular
  .module('travelsApp')
  .run(['$rootScope','oauth',function($rootScope,oauth) {
    $rootScope.urlPrefix = APIURL;


  }])
  .config(function(LoopBackResourceProvider,$httpProvider) {

    // Use a custom auth header instead of the default 'Authorization'
    LoopBackResourceProvider.setAuthHeader('X-Access-Token');

    // Change the URL where to access the LoopBack REST API server
    LoopBackResourceProvider.setUrlBase(APIURL);

    $httpProvider.interceptors.push(function($q, $location, LoopBackAuth) {
      return {
        responseError: function(rejection) {
          if (rejection.status == 401) {
            //Now clearing the loopback values from client browser for safe logout...
            LoopBackAuth.clearUser();
            LoopBackAuth.clearStorage();
            $location.nextAfterLogin = $location.path();
            $location.path('/');
          }
          return $q.reject(rejection);
        }
      };
    });
  });


'use strict';

angular.module('travelsApp')
    .provider('urls', function () {
        // In the provider function, you cannot inject any
        // service or factory. This can only be done at the
        // "$get" method.

        this.login = '/';
        this.loginUrl = '/login';
        this.logout = '/logout';


        this.$get = function () {
            var login = this.login;
            var logout = this.logout;


            var loginUrl = this.loginUrl;
            return {
                login: function () {
                    return login;
                },
                logout: function () {
                    return logout;
                },

                loginUrl: function () {
                    return loginUrl;
                }
            }
        };

        this.setLogin = function (url) {
            this.login = url;
        };

        this.setLogout = function (url) {
            this.logout = url;
        };

        this.setLoginUrl = function (url) {
            this.loginUrl = url;
        };

    })
    .config(['urlsProvider', function (urlsProvider) {
        var APIURL = 'https://mighty-garden-62797.herokuapp.com';

        urlsProvider.setLogin(APIURL + '/secret');
        urlsProvider.setLogout(APIURL + '/logout');
        // urlsProvider.setLogin('http://soyosung/api/Secret/');
    }])
    .factory('addToken', ['$q', 'currentUser', "$location", "$rootScope",
        function ($q, currentUser, $location, $rootScope) {


            var request = function (config) {

                if (currentUser.profile.loggedIn) {
                    config.headers.Authorization = currentUser.profile.token;
                    $rootScope.logged = true;
                }
                else {
                    $location.path("/");
                }
                /*if*/

                return $q.when(config);
            }

            return {
                request: request
            };

        }
    ])
    .config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push("addToken");
    }])
    .factory('currentUser', ['localStorage', '$window', function (localStorage, $window) {
        // Service logic
        // ...

        var USERKEY = 'utoken';


        var setProfile = function (employeeID, token, role, fullName, clientID) {
            profile.employeeID = employeeID;
            profile.token = token;
            profile.role = role;
            profile.fullName = fullName;
            profile.clientID = clientID;
            profile.currency = currency;
            this.profile.employeeID = employeeID;
            this.profile.token = token;
            this.profile.role = role;
            this.profile.fullName = fullName;
            this.profile.clientID = clientID;
            this.profile.currency = currency;

            localStorage.add(USERKEY, profile);
        };

        var setCurrency = function (currencySelected) {
            currency.selectedCurrency = currencySelected;
            this.currency.selectedCurrency = currencySelected;

            localStorage.add("currency", currency);
        };

        var initialize = function () {
            var user = {
                employeeID: "",
                token: "",
                role: [],
                fullName: "",
                clientID: "",
                currency: "",
                get loggedIn() {
                    return this.token;
                }
            };

            var localUser = localStorage.get(USERKEY);
            if (localUser) {
                user.employeeID = localUser.employeeID;
                user.token = localUser.token;
                user.clientID = localUser.clientID;
                user.role = localUser.role;
                user.fullName = localUser.fullName;
                user.currency = localUser.currency;
            }
            return user;
        };
        var initializeCurrency = function () {
            var currency = {
                defaultCurrency: "MXN",
                selectedCurrency: ""
            }

            var localCurrency = localStorage.get("currency");
            if (localCurrency) {
                currency.selectedCurrency = localCurrency.selectedCurrency;
                currency.defaultCurrency = localCurrency.defaultCurrency;
            }
            return currency;
        };


        var currency = initializeCurrency();
        var profile = initialize();


        return {
            setCurrency: setCurrency,
            currency: currency,
            setProfile: setProfile,
            profile: profile,

        };
    }])
    .factory('oauth', ['$http', '$location', 'currentUser', 'urls',
        function ($http, $location, currentUser, urls) {
            // Service logic
            // ...


            var meaningOfLife = 42;
            var login = function (username, password) {

                var data = {
                    username: username,
                    password: password,
                    appID: urls.appId()
                };
                //return $http.post(urls.login(), data)
                return $http.post(urls.login(), data)
                    .then(function (response) {
                        console.log(response);

                        return response;


                    });
            };


            var logout = function () {
                var clientID = currentUser.profile.clientID,
                    username = currentUser.profile.username,
                    key = currentUser.profile.token;

                currentUser.setProfile(null, null, null, null, null);


                $location.path(urls.loginUrl());
                var data = {
                    appID: urls.appId(),
                    clientId: clientID,
                    username: username,
                    key: key
                };
                $http.post(urls.logout(), data)
                    .then(function (response) {
                        alert("You've been logged out .");
                        localStorage.clear();
                    }, function (error) {
                        alert("Unable to log out ");
                        localStorage.clear();
                    });
            };

            var checkPermissionForView = function (view) {
                if (!view.requiresAuthentication) {
                    return true;
                }

                return userHasPermissionForView(view);
            };


            var userHasPermissionForView = function (view) {
                console.log(currentUser.profile.loggedIn);

                if (!currentUser.profile.loggedIn) {
                    return false;
                }

                if (!view.permissions || !view.permissions.length) {
                    return true;
                }

                return userHasPermission(view.permissions);
            };


            var userHasPermission = function (permissions) {
                if (!currentUser.profile.loggedIn) {
                    return false;
                }

                var found = false;
                angular.forEach(permissions, function (permission, index) {
                    if (currentUser.profile.role.indexOf(permission) >= 0) {


                        found = true;
                        return;
                    }
                });

                return found;
            };


            var isLoggedIn = (currentUser.profile.loggedIn != null &&
            currentUser.profile.loggedIn != undefined &&
            currentUser.profile.loggedIn != "");
            // Public API here
            return {
                login: login,
                logout: logout,
                checkPermissionForView: checkPermissionForView,
                userHasPermission: userHasPermission,
                isLoggedIn: isLoggedIn,

            };
        }
    ])
    .factory('localStorage', ['$window', function ($window) {
        // Service logic
        // ...

        var meaningOfLife = 42;
        var store = $window.localStorage;

        var add = function (key, value) {
            value = angular.toJson(value);
            store.setItem(key, value);
        };

        var get = function (key) {
            var value = store.getItem(key);
            if (value) {
                value = angular.fromJson(value);
            }
            return value;
        };

        var remove = function (key) {
            store.removeItem(key);
        };

        var clear = function () {
            store.clear();
        };

        // Public API here
        return {
            add: add,
            get: get,
            remove: remove,
            clear: clear
        };
    }]);

'use strict';

/**
 * @ngdoc function
 * @name travelsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the travelsApp
 */
angular.module('travelsApp')
  .controller('MainCtrl', function($rootScope, Account, Travel, $location, $state, oauth, currentUser,toaster) {
    var vm = this;
    $rootScope.logged = false;
    currentUser.setProfile(null, null, null, null, null);

    vm.isSending = false;
    /**
     * @desc function forlogin
     * @param url
     */
    vm.login = function() {
      vm.isSending = true;
      console.log(vm.user);
      Account.login(vm.user,
        function(res) {
          vm.isSending = false;
          console.log(res);
          $state.go("dashboard");
          $rootScope.logged = true;
          currentUser.setProfile(null, res.id, null, null, res.userId);
          currentUser.setCurrency("MXN");
          console.log("location dashboard");
        },
        function(err) {
          toaster.pop("error","Invalid credentials try again");
          vm.isSending = false;
          console.log(err);
        })
    };




  });

// CommonJS package manager support
if (typeof module !== 'undefined' && typeof exports !== 'undefined' &&
  module.exports === exports) {
  // Export the *name* of this Angular module
  // Sample usage:
  //
  //   import lbServices from './lb-services';
  //   angular.module('app', [lbServices]);
  //
  module.exports = "lbServices";
}

(function(window, angular, undefined) {
  'use strict';

  var urlBase = "/api";
  var authHeader = 'authorization';

  function getHost(url) {
    var m = url.match(/^(?:https?:)?\/\/([^\/]+)/);
    return m ? m[1] : null;
  }

  var urlBaseHost = getHost(urlBase) || location.host;

/**
 * @ngdoc overview
 * @name lbServices
 * @module
 * @description
 *
 * The `lbServices` module provides services for interacting with
 * the models exposed by the LoopBack server via the REST API.
 *
 */
  var module = angular.module("lbServices", ['ngResource']);

/**
 * @ngdoc object
 * @name lbServices.Employee
 * @header lbServices.Employee
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Employee` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Employee",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/employees/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Employee.travels.findById() instead.
            "prototype$__findById__travels": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/employees/:id/travels/:fk",
              method: "GET",
            },

            // INTERNAL. Use Employee.travels.destroyById() instead.
            "prototype$__destroyById__travels": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/employees/:id/travels/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Employee.travels.updateById() instead.
            "prototype$__updateById__travels": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/employees/:id/travels/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Employee.expenses.findById() instead.
            "prototype$__findById__expenses": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/employees/:id/expenses/:fk",
              method: "GET",
            },

            // INTERNAL. Use Employee.expenses.destroyById() instead.
            "prototype$__destroyById__expenses": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/employees/:id/expenses/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Employee.expenses.updateById() instead.
            "prototype$__updateById__expenses": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/employees/:id/expenses/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Employee.account() instead.
            "prototype$__get__account": {
              url: urlBase + "/employees/:id/account",
              method: "GET",
            },

            // INTERNAL. Use Employee.travels() instead.
            "prototype$__get__travels": {
              isArray: true,
              url: urlBase + "/employees/:id/travels",
              method: "GET",
            },

            // INTERNAL. Use Employee.travels.create() instead.
            "prototype$__create__travels": {
              url: urlBase + "/employees/:id/travels",
              method: "POST",
            },

            // INTERNAL. Use Employee.travels.destroyAll() instead.
            "prototype$__delete__travels": {
              url: urlBase + "/employees/:id/travels",
              method: "DELETE",
            },

            // INTERNAL. Use Employee.travels.count() instead.
            "prototype$__count__travels": {
              url: urlBase + "/employees/:id/travels/count",
              method: "GET",
            },

            // INTERNAL. Use Employee.expenses() instead.
            "prototype$__get__expenses": {
              isArray: true,
              url: urlBase + "/employees/:id/expenses",
              method: "GET",
            },

            // INTERNAL. Use Employee.expenses.create() instead.
            "prototype$__create__expenses": {
              url: urlBase + "/employees/:id/expenses",
              method: "POST",
            },

            // INTERNAL. Use Employee.expenses.destroyAll() instead.
            "prototype$__delete__expenses": {
              url: urlBase + "/employees/:id/expenses",
              method: "DELETE",
            },

            // INTERNAL. Use Employee.expenses.count() instead.
            "prototype$__count__expenses": {
              url: urlBase + "/employees/:id/expenses/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Employee#create
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/employees",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Employee#createMany
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/employees",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Employee#patchOrCreate
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/employees",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Employee#replaceOrCreate
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/employees/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Employee#upsertWithWhere
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/employees/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Employee#exists
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/employees/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Employee#findById
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/employees/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Employee#replaceById
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/employees/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Employee#find
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/employees",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Employee#findOne
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/employees/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Employee#updateAll
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/employees/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Employee#deleteById
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/employees/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Employee#count
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/employees/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Employee#prototype$patchAttributes
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Employee id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/employees/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Employee#createChangeStream
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/employees/change-stream",
              method: "POST",
            },

            // INTERNAL. Use Account.employee() instead.
            "::get::Account::employee": {
              url: urlBase + "/accounts/:id/employee",
              method: "GET",
            },

            // INTERNAL. Use Account.employee.create() instead.
            "::create::Account::employee": {
              url: urlBase + "/accounts/:id/employee",
              method: "POST",
            },

            // INTERNAL. Use Account.employee.createMany() instead.
            "::createMany::Account::employee": {
              isArray: true,
              url: urlBase + "/accounts/:id/employee",
              method: "POST",
            },

            // INTERNAL. Use Account.employee.update() instead.
            "::update::Account::employee": {
              url: urlBase + "/accounts/:id/employee",
              method: "PUT",
            },

            // INTERNAL. Use Account.employee.destroy() instead.
            "::destroy::Account::employee": {
              url: urlBase + "/accounts/:id/employee",
              method: "DELETE",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Employee#upsert
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Employee#updateOrCreate
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Employee#patchOrCreateWithWhere
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Employee#update
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Employee#destroyById
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Employee#removeById
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Employee#updateAttributes
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Employee id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
        R["updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Employee#modelName
        * @propertyOf lbServices.Employee
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Employee`.
        */
        R.modelName = "Employee";

    /**
     * @ngdoc object
     * @name lbServices.Employee.travels
     * @header lbServices.Employee.travels
     * @object
     * @description
     *
     * The object `Employee.travels` groups methods
     * manipulating `Travel` instances related to `Employee`.
     *
     * Call {@link lbServices.Employee#travels Employee.travels()}
     * to query all related instances.
     */


            /**
             * @ngdoc method
             * @name lbServices.Employee#travels
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Queries travels of Employee.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Employee id
             *
             *  - `options` – `{object=}` -
             *
             *  - `filter` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
        R.travels = function() {
          var TargetResource = $injector.get("Travel");
          var action = TargetResource["::get::Employee::travels"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Employee.travels#count
             * @methodOf lbServices.Employee.travels
             *
             * @description
             *
             * Counts travels of Employee.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Employee id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
        R.travels.count = function() {
          var TargetResource = $injector.get("Travel");
          var action = TargetResource["::count::Employee::travels"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Employee.travels#create
             * @methodOf lbServices.Employee.travels
             *
             * @description
             *
             * Creates a new instance in travels of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Employee id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
        R.travels.create = function() {
          var TargetResource = $injector.get("Travel");
          var action = TargetResource["::create::Employee::travels"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Employee.travels#createMany
             * @methodOf lbServices.Employee.travels
             *
             * @description
             *
             * Creates a new instance in travels of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Employee id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
        R.travels.createMany = function() {
          var TargetResource = $injector.get("Travel");
          var action = TargetResource["::createMany::Employee::travels"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Employee.travels#destroyAll
             * @methodOf lbServices.Employee.travels
             *
             * @description
             *
             * Deletes all travels of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Employee id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.travels.destroyAll = function() {
          var TargetResource = $injector.get("Travel");
          var action = TargetResource["::delete::Employee::travels"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Employee.travels#destroyById
             * @methodOf lbServices.Employee.travels
             *
             * @description
             *
             * Delete a related item by id for travels.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Employee id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for travels
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.travels.destroyById = function() {
          var TargetResource = $injector.get("Travel");
          var action = TargetResource["::destroyById::Employee::travels"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Employee.travels#findById
             * @methodOf lbServices.Employee.travels
             *
             * @description
             *
             * Find a related item by id for travels.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Employee id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for travels
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
        R.travels.findById = function() {
          var TargetResource = $injector.get("Travel");
          var action = TargetResource["::findById::Employee::travels"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Employee.travels#updateById
             * @methodOf lbServices.Employee.travels
             *
             * @description
             *
             * Update a related item by id for travels.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Employee id
             *
             *  - `fk` – `{*}` - Foreign key for travels
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
        R.travels.updateById = function() {
          var TargetResource = $injector.get("Travel");
          var action = TargetResource["::updateById::Employee::travels"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Employee.expenses
     * @header lbServices.Employee.expenses
     * @object
     * @description
     *
     * The object `Employee.expenses` groups methods
     * manipulating `Expense` instances related to `Employee`.
     *
     * Call {@link lbServices.Employee#expenses Employee.expenses()}
     * to query all related instances.
     */


            /**
             * @ngdoc method
             * @name lbServices.Employee#expenses
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Queries expenses of Employee.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Employee id
             *
             *  - `options` – `{object=}` -
             *
             *  - `filter` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
        R.expenses = function() {
          var TargetResource = $injector.get("Expense");
          var action = TargetResource["::get::Employee::expenses"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Employee.expenses#count
             * @methodOf lbServices.Employee.expenses
             *
             * @description
             *
             * Counts expenses of Employee.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Employee id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
        R.expenses.count = function() {
          var TargetResource = $injector.get("Expense");
          var action = TargetResource["::count::Employee::expenses"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Employee.expenses#create
             * @methodOf lbServices.Employee.expenses
             *
             * @description
             *
             * Creates a new instance in expenses of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Employee id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
        R.expenses.create = function() {
          var TargetResource = $injector.get("Expense");
          var action = TargetResource["::create::Employee::expenses"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Employee.expenses#createMany
             * @methodOf lbServices.Employee.expenses
             *
             * @description
             *
             * Creates a new instance in expenses of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Employee id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
        R.expenses.createMany = function() {
          var TargetResource = $injector.get("Expense");
          var action = TargetResource["::createMany::Employee::expenses"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Employee.expenses#destroyAll
             * @methodOf lbServices.Employee.expenses
             *
             * @description
             *
             * Deletes all expenses of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Employee id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.expenses.destroyAll = function() {
          var TargetResource = $injector.get("Expense");
          var action = TargetResource["::delete::Employee::expenses"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Employee.expenses#destroyById
             * @methodOf lbServices.Employee.expenses
             *
             * @description
             *
             * Delete a related item by id for expenses.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Employee id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for expenses
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.expenses.destroyById = function() {
          var TargetResource = $injector.get("Expense");
          var action = TargetResource["::destroyById::Employee::expenses"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Employee.expenses#findById
             * @methodOf lbServices.Employee.expenses
             *
             * @description
             *
             * Find a related item by id for expenses.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Employee id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for expenses
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
        R.expenses.findById = function() {
          var TargetResource = $injector.get("Expense");
          var action = TargetResource["::findById::Employee::expenses"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Employee.expenses#updateById
             * @methodOf lbServices.Employee.expenses
             *
             * @description
             *
             * Update a related item by id for expenses.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Employee id
             *
             *  - `fk` – `{*}` - Foreign key for expenses
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
        R.expenses.updateById = function() {
          var TargetResource = $injector.get("Expense");
          var action = TargetResource["::updateById::Employee::expenses"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Employee#account
             * @methodOf lbServices.Employee
             *
             * @description
             *
             * Fetches belongsTo relation account.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Employee id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
        R.account = function() {
          var TargetResource = $injector.get("Account");
          var action = TargetResource["::get::Employee::account"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Travel
 * @header lbServices.Travel
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Travel` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Travel",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/travels/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Travel.expenses.findById() instead.
            "prototype$__findById__expenses": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/travels/:id/expenses/:fk",
              method: "GET",
            },

            // INTERNAL. Use Travel.expenses.destroyById() instead.
            "prototype$__destroyById__expenses": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/travels/:id/expenses/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Travel.expenses.updateById() instead.
            "prototype$__updateById__expenses": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/travels/:id/expenses/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Travel.expenses() instead.
            "prototype$__get__expenses": {
              isArray: true,
              url: urlBase + "/travels/:id/expenses",
              method: "GET",
            },

            // INTERNAL. Use Travel.expenses.create() instead.
            "prototype$__create__expenses": {
              url: urlBase + "/travels/:id/expenses",
              method: "POST",
            },

            // INTERNAL. Use Travel.expenses.destroyAll() instead.
            "prototype$__delete__expenses": {
              url: urlBase + "/travels/:id/expenses",
              method: "DELETE",
            },

            // INTERNAL. Use Travel.expenses.count() instead.
            "prototype$__count__expenses": {
              url: urlBase + "/travels/:id/expenses/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Travel#create
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/travels",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Travel#createMany
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/travels",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Travel#patchOrCreate
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/travels",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Travel#replaceOrCreate
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/travels/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Travel#upsertWithWhere
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/travels/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Travel#exists
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/travels/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Travel#findById
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/travels/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Travel#replaceById
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/travels/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Travel#find
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/travels",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Travel#findOne
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/travels/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Travel#updateAll
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/travels/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Travel#deleteById
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/travels/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Travel#count
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/travels/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Travel#prototype$patchAttributes
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Travel id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/travels/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Travel#createChangeStream
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/travels/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Travel#totalOfExpenses
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Method used to obtain how much is registered on expenses
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `idTravel` – `{string}` - Argument required to findById the travel
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `ammount` – `{number=}` - Ammount of the cost of all expenses of tha travel
             */
            "totalOfExpenses": {
              url: urlBase + "/travels/:idTravel/totalExpenses",
              method: "GET",
            },

            // INTERNAL. Use Employee.travels.findById() instead.
            "::findById::Employee::travels": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/employees/:id/travels/:fk",
              method: "GET",
            },

            // INTERNAL. Use Employee.travels.destroyById() instead.
            "::destroyById::Employee::travels": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/employees/:id/travels/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Employee.travels.updateById() instead.
            "::updateById::Employee::travels": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/employees/:id/travels/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Employee.travels() instead.
            "::get::Employee::travels": {
              isArray: true,
              url: urlBase + "/employees/:id/travels",
              method: "GET",
            },

            // INTERNAL. Use Employee.travels.create() instead.
            "::create::Employee::travels": {
              url: urlBase + "/employees/:id/travels",
              method: "POST",
            },

            // INTERNAL. Use Employee.travels.createMany() instead.
            "::createMany::Employee::travels": {
              isArray: true,
              url: urlBase + "/employees/:id/travels",
              method: "POST",
            },

            // INTERNAL. Use Employee.travels.destroyAll() instead.
            "::delete::Employee::travels": {
              url: urlBase + "/employees/:id/travels",
              method: "DELETE",
            },

            // INTERNAL. Use Employee.travels.count() instead.
            "::count::Employee::travels": {
              url: urlBase + "/employees/:id/travels/count",
              method: "GET",
            },

            // INTERNAL. Use Expense.travel() instead.
            "::get::Expense::travel": {
              url: urlBase + "/expenses/:id/travel",
              method: "GET",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Travel#upsert
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Travel#updateOrCreate
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Travel#patchOrCreateWithWhere
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Travel#update
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Travel#destroyById
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Travel#removeById
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Travel#updateAttributes
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Travel id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
        R["updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Travel#modelName
        * @propertyOf lbServices.Travel
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Travel`.
        */
        R.modelName = "Travel";

    /**
     * @ngdoc object
     * @name lbServices.Travel.expenses
     * @header lbServices.Travel.expenses
     * @object
     * @description
     *
     * The object `Travel.expenses` groups methods
     * manipulating `Expense` instances related to `Travel`.
     *
     * Call {@link lbServices.Travel#expenses Travel.expenses()}
     * to query all related instances.
     */


            /**
             * @ngdoc method
             * @name lbServices.Travel#expenses
             * @methodOf lbServices.Travel
             *
             * @description
             *
             * Queries expenses of Travel.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Travel id
             *
             *  - `options` – `{object=}` -
             *
             *  - `filter` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
        R.expenses = function() {
          var TargetResource = $injector.get("Expense");
          var action = TargetResource["::get::Travel::expenses"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Travel.expenses#count
             * @methodOf lbServices.Travel.expenses
             *
             * @description
             *
             * Counts expenses of Travel.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Travel id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
        R.expenses.count = function() {
          var TargetResource = $injector.get("Expense");
          var action = TargetResource["::count::Travel::expenses"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Travel.expenses#create
             * @methodOf lbServices.Travel.expenses
             *
             * @description
             *
             * Creates a new instance in expenses of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Travel id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
        R.expenses.create = function() {
          var TargetResource = $injector.get("Expense");
          var action = TargetResource["::create::Travel::expenses"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Travel.expenses#createMany
             * @methodOf lbServices.Travel.expenses
             *
             * @description
             *
             * Creates a new instance in expenses of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Travel id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
        R.expenses.createMany = function() {
          var TargetResource = $injector.get("Expense");
          var action = TargetResource["::createMany::Travel::expenses"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Travel.expenses#destroyAll
             * @methodOf lbServices.Travel.expenses
             *
             * @description
             *
             * Deletes all expenses of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Travel id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.expenses.destroyAll = function() {
          var TargetResource = $injector.get("Expense");
          var action = TargetResource["::delete::Travel::expenses"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Travel.expenses#destroyById
             * @methodOf lbServices.Travel.expenses
             *
             * @description
             *
             * Delete a related item by id for expenses.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Travel id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for expenses
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.expenses.destroyById = function() {
          var TargetResource = $injector.get("Expense");
          var action = TargetResource["::destroyById::Travel::expenses"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Travel.expenses#findById
             * @methodOf lbServices.Travel.expenses
             *
             * @description
             *
             * Find a related item by id for expenses.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Travel id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for expenses
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
        R.expenses.findById = function() {
          var TargetResource = $injector.get("Expense");
          var action = TargetResource["::findById::Travel::expenses"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Travel.expenses#updateById
             * @methodOf lbServices.Travel.expenses
             *
             * @description
             *
             * Update a related item by id for expenses.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Travel id
             *
             *  - `fk` – `{*}` - Foreign key for expenses
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
        R.expenses.updateById = function() {
          var TargetResource = $injector.get("Expense");
          var action = TargetResource["::updateById::Travel::expenses"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Expense
 * @header lbServices.Expense
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Expense` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Expense",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/expenses/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Expense.travel() instead.
            "prototype$__get__travel": {
              url: urlBase + "/expenses/:id/travel",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Expense#create
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/expenses",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Expense#createMany
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/expenses",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Expense#patchOrCreate
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/expenses",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Expense#replaceOrCreate
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/expenses/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Expense#upsertWithWhere
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/expenses/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Expense#exists
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/expenses/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Expense#findById
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/expenses/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Expense#replaceById
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/expenses/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Expense#find
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/expenses",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Expense#findOne
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/expenses/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Expense#updateAll
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/expenses/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Expense#deleteById
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/expenses/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Expense#count
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/expenses/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Expense#prototype$patchAttributes
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Expense id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/expenses/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Expense#createChangeStream
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/expenses/change-stream",
              method: "POST",
            },

            // INTERNAL. Use Employee.expenses.findById() instead.
            "::findById::Employee::expenses": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/employees/:id/expenses/:fk",
              method: "GET",
            },

            // INTERNAL. Use Employee.expenses.destroyById() instead.
            "::destroyById::Employee::expenses": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/employees/:id/expenses/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Employee.expenses.updateById() instead.
            "::updateById::Employee::expenses": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/employees/:id/expenses/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Employee.expenses() instead.
            "::get::Employee::expenses": {
              isArray: true,
              url: urlBase + "/employees/:id/expenses",
              method: "GET",
            },

            // INTERNAL. Use Employee.expenses.create() instead.
            "::create::Employee::expenses": {
              url: urlBase + "/employees/:id/expenses",
              method: "POST",
            },

            // INTERNAL. Use Employee.expenses.createMany() instead.
            "::createMany::Employee::expenses": {
              isArray: true,
              url: urlBase + "/employees/:id/expenses",
              method: "POST",
            },

            // INTERNAL. Use Employee.expenses.destroyAll() instead.
            "::delete::Employee::expenses": {
              url: urlBase + "/employees/:id/expenses",
              method: "DELETE",
            },

            // INTERNAL. Use Employee.expenses.count() instead.
            "::count::Employee::expenses": {
              url: urlBase + "/employees/:id/expenses/count",
              method: "GET",
            },

            // INTERNAL. Use Travel.expenses.findById() instead.
            "::findById::Travel::expenses": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/travels/:id/expenses/:fk",
              method: "GET",
            },

            // INTERNAL. Use Travel.expenses.destroyById() instead.
            "::destroyById::Travel::expenses": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/travels/:id/expenses/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Travel.expenses.updateById() instead.
            "::updateById::Travel::expenses": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/travels/:id/expenses/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Travel.expenses() instead.
            "::get::Travel::expenses": {
              isArray: true,
              url: urlBase + "/travels/:id/expenses",
              method: "GET",
            },

            // INTERNAL. Use Travel.expenses.create() instead.
            "::create::Travel::expenses": {
              url: urlBase + "/travels/:id/expenses",
              method: "POST",
            },

            // INTERNAL. Use Travel.expenses.createMany() instead.
            "::createMany::Travel::expenses": {
              isArray: true,
              url: urlBase + "/travels/:id/expenses",
              method: "POST",
            },

            // INTERNAL. Use Travel.expenses.destroyAll() instead.
            "::delete::Travel::expenses": {
              url: urlBase + "/travels/:id/expenses",
              method: "DELETE",
            },

            // INTERNAL. Use Travel.expenses.count() instead.
            "::count::Travel::expenses": {
              url: urlBase + "/travels/:id/expenses/count",
              method: "GET",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Expense#upsert
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Expense#updateOrCreate
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Expense#patchOrCreateWithWhere
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Expense#update
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Expense#destroyById
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Expense#removeById
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Expense#updateAttributes
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Expense id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Expense` object.)
             * </em>
             */
        R["updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Expense#modelName
        * @propertyOf lbServices.Expense
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Expense`.
        */
        R.modelName = "Expense";


            /**
             * @ngdoc method
             * @name lbServices.Expense#travel
             * @methodOf lbServices.Expense
             *
             * @description
             *
             * Fetches belongsTo relation travel.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Expense id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Travel` object.)
             * </em>
             */
        R.travel = function() {
          var TargetResource = $injector.get("Travel");
          var action = TargetResource["::get::Expense::travel"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Account
 * @header lbServices.Account
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Account` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Account",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/accounts/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Account.employee() instead.
            "prototype$__get__employee": {
              url: urlBase + "/accounts/:id/employee",
              method: "GET",
            },

            // INTERNAL. Use Account.employee.create() instead.
            "prototype$__create__employee": {
              url: urlBase + "/accounts/:id/employee",
              method: "POST",
            },

            // INTERNAL. Use Account.employee.update() instead.
            "prototype$__update__employee": {
              url: urlBase + "/accounts/:id/employee",
              method: "PUT",
            },

            // INTERNAL. Use Account.employee.destroy() instead.
            "prototype$__destroy__employee": {
              url: urlBase + "/accounts/:id/employee",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#prototype$__findById__accessTokens
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Find a related item by id for accessTokens.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Account id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for accessTokens
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "prototype$__findById__accessTokens": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/accounts/:id/accessTokens/:fk",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#prototype$__destroyById__accessTokens
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Delete a related item by id for accessTokens.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Account id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for accessTokens
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "prototype$__destroyById__accessTokens": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/accounts/:id/accessTokens/:fk",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#prototype$__updateById__accessTokens
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Update a related item by id for accessTokens.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Account id
             *
             *  - `fk` – `{*}` - Foreign key for accessTokens
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "prototype$__updateById__accessTokens": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/accounts/:id/accessTokens/:fk",
              method: "PUT",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#prototype$__get__accessTokens
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Queries accessTokens of Account.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Account id
             *
             *  - `options` – `{object=}` -
             *
             *  - `filter` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "prototype$__get__accessTokens": {
              isArray: true,
              url: urlBase + "/accounts/:id/accessTokens",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#prototype$__create__accessTokens
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Creates a new instance in accessTokens of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Account id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "prototype$__create__accessTokens": {
              url: urlBase + "/accounts/:id/accessTokens",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#prototype$__delete__accessTokens
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Deletes all accessTokens of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Account id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "prototype$__delete__accessTokens": {
              url: urlBase + "/accounts/:id/accessTokens",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#prototype$__count__accessTokens
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Counts accessTokens of Account.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Account id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "prototype$__count__accessTokens": {
              url: urlBase + "/accounts/:id/accessTokens/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#create
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/accounts",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#createMany
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/accounts",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#patchOrCreate
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/accounts",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#replaceOrCreate
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/accounts/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#upsertWithWhere
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/accounts/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#exists
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/accounts/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#findById
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/accounts/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#replaceById
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/accounts/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#find
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/accounts",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#findOne
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/accounts/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#updateAll
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/accounts/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#deleteById
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/accounts/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#count
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/accounts/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#prototype$patchAttributes
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Account id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/accounts/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#createChangeStream
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/accounts/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#login
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Login a user with username/email and password.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `include` – `{string=}` - Related objects to include in the response. See the description of return value for more details.
             *   Default value: `user`.
             *
             *  - `rememberMe` - `boolean` - Whether the authentication credentials
             *     should be remembered in localStorage across app/browser restarts.
             *     Default: `true`.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * The response body contains properties of the AccessToken created on login.
             * Depending on the value of `include` parameter, the body may contain additional properties:
             *   - `user` - `U+007BUserU+007D` - Data of the currently logged in user. (`include=user`)
             *
             */
            "login": {
              params: {
                include: 'user',
              },
              interceptor: {
                response: function(response) {
                  var accessToken = response.data;
                  LoopBackAuth.setUser(
                    accessToken.id, accessToken.userId, accessToken.user);
                  LoopBackAuth.rememberMe =
                    response.config.params.rememberMe !== false;
                  LoopBackAuth.save();
                  return response.resource;
                },
              },
              url: urlBase + "/accounts/login",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#logout
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Logout a user with access token.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `access_token` – `{string=}` - Do not supply this argument, it is automatically extracted from request headers.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "logout": {
              interceptor: {
                response: function(response) {
                  LoopBackAuth.clearUser();
                  LoopBackAuth.clearStorage();
                  return response.resource;
                },
                responseError: function(responseError) {
                  LoopBackAuth.clearUser();
                  LoopBackAuth.clearStorage();
                  return responseError.resource;
                },
              },
              url: urlBase + "/accounts/logout",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#prototype$verify
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Trigger user's identity verification with configured verifyOptions
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Account id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `verifyOptions` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "prototype$verify": {
              url: urlBase + "/accounts/:id/verify",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#confirm
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Confirm a user registration with identity verification token.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `uid` – `{string}` -
             *
             *  - `token` – `{string}` -
             *
             *  - `redirect` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "confirm": {
              url: urlBase + "/accounts/confirm",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#resetPassword
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Reset password for a user with email.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "resetPassword": {
              url: urlBase + "/accounts/reset",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#changePassword
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Change a user's password.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `id` – `{*=}` -
             *
             *  - `oldPassword` – `{string}` -
             *
             *  - `newPassword` – `{string}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "changePassword": {
              url: urlBase + "/accounts/change-password",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#setPassword
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Reset user's password via a password-reset token.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `id` – `{*=}` -
             *
             *  - `newPassword` – `{string}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "setPassword": {
              url: urlBase + "/accounts/reset-password",
              method: "POST",
            },

            // INTERNAL. Use Employee.account() instead.
            "::get::Employee::account": {
              url: urlBase + "/employees/:id/account",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#getCurrent
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Get data of the currently logged user. Fail with HTTP result 401
             * when there is no user logged in.
             *
             * @param {function(Object,Object)=} successCb
             *    Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *    `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             */
            'getCurrent': {
              url: urlBase + "/accounts" + '/:id',
              method: 'GET',
              params: {
                id: function() {
                  var id = LoopBackAuth.currentUserId;
                  if (id == null) id = '__anonymous__';
                  return id;
                },
              },
              interceptor: {
                response: function(response) {
                  LoopBackAuth.currentUserData = response.data;
                  return response.resource;
                },
                responseError: function(responseError) {
                  LoopBackAuth.clearUser();
                  LoopBackAuth.clearStorage();
                  return $q.reject(responseError);
                },
              },
              __isGetCurrentUser__: true,
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Account#upsert
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Account#updateOrCreate
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Account#patchOrCreateWithWhere
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Account#update
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Account#destroyById
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Account#removeById
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Account#updateAttributes
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Account id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
        R["updateAttributes"] = R["prototype$patchAttributes"];

        /**
         * @ngdoc method
         * @name lbServices.Account#getCachedCurrent
         * @methodOf lbServices.Account
         *
         * @description
         *
         * Get data of the currently logged user that was returned by the last
         * call to {@link lbServices.Account#login} or
         * {@link lbServices.Account#getCurrent}. Return null when there
         * is no user logged in or the data of the current user were not fetched
         * yet.
         *
         * @returns {Object} A Account instance.
         */
        R.getCachedCurrent = function() {
          var data = LoopBackAuth.currentUserData;
          return data ? new R(data) : null;
        };

        /**
         * @ngdoc method
         * @name lbServices.Account#isAuthenticated
         * @methodOf lbServices.Account
         *
         * @returns {boolean} True if the current user is authenticated (logged in).
         */
        R.isAuthenticated = function() {
          return this.getCurrentId() != null;
        };

        /**
         * @ngdoc method
         * @name lbServices.Account#getCurrentId
         * @methodOf lbServices.Account
         *
         * @returns {Object} Id of the currently logged-in user or null.
         */
        R.getCurrentId = function() {
          return LoopBackAuth.currentUserId;
        };

        /**
        * @ngdoc property
        * @name lbServices.Account#modelName
        * @propertyOf lbServices.Account
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Account`.
        */
        R.modelName = "Account";

    /**
     * @ngdoc object
     * @name lbServices.Account.employee
     * @header lbServices.Account.employee
     * @object
     * @description
     *
     * The object `Account.employee` groups methods
     * manipulating `Employee` instances related to `Account`.
     *
     * Call {@link lbServices.Account#employee Account.employee()}
     * to query all related instances.
     */


            /**
             * @ngdoc method
             * @name lbServices.Account#employee
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Fetches hasOne relation employee.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Account id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
        R.employee = function() {
          var TargetResource = $injector.get("Employee");
          var action = TargetResource["::get::Account::employee"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Account.employee#create
             * @methodOf lbServices.Account.employee
             *
             * @description
             *
             * Creates a new instance in employee of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Account id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
        R.employee.create = function() {
          var TargetResource = $injector.get("Employee");
          var action = TargetResource["::create::Account::employee"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Account.employee#createMany
             * @methodOf lbServices.Account.employee
             *
             * @description
             *
             * Creates a new instance in employee of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Account id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
        R.employee.createMany = function() {
          var TargetResource = $injector.get("Employee");
          var action = TargetResource["::createMany::Account::employee"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Account.employee#destroy
             * @methodOf lbServices.Account.employee
             *
             * @description
             *
             * Deletes employee of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Account id
             *
             *  - `options` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.employee.destroy = function() {
          var TargetResource = $injector.get("Employee");
          var action = TargetResource["::destroy::Account::employee"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Account.employee#update
             * @methodOf lbServices.Account.employee
             *
             * @description
             *
             * Update employee of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Account id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Employee` object.)
             * </em>
             */
        R.employee.update = function() {
          var TargetResource = $injector.get("Employee");
          var action = TargetResource["::update::Account::employee"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Container
 * @header lbServices.Container
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Container` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Container",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/containers/:id",
          { 'id': '@id' },
          {

            /**
             * @ngdoc method
             * @name lbServices.Container#getContainers
             * @methodOf lbServices.Container
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Container` object.)
             * </em>
             */
            "getContainers": {
              isArray: true,
              url: urlBase + "/containers",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Container#createContainer
             * @methodOf lbServices.Container
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Container` object.)
             * </em>
             */
            "createContainer": {
              url: urlBase + "/containers",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Container#destroyContainer
             * @methodOf lbServices.Container
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `container` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `` – `{undefined=}` -
             */
            "destroyContainer": {
              url: urlBase + "/containers/:container",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Container#getContainer
             * @methodOf lbServices.Container
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `container` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Container` object.)
             * </em>
             */
            "getContainer": {
              url: urlBase + "/containers/:container",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Container#getFiles
             * @methodOf lbServices.Container
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `container` – `{string=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Container` object.)
             * </em>
             */
            "getFiles": {
              isArray: true,
              url: urlBase + "/containers/:container/files",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Container#getFile
             * @methodOf lbServices.Container
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `container` – `{string=}` -
             *
             *  - `file` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Container` object.)
             * </em>
             */
            "getFile": {
              url: urlBase + "/containers/:container/files/:file",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Container#removeFile
             * @methodOf lbServices.Container
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `container` – `{string=}` -
             *
             *  - `file` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `` – `{undefined=}` -
             */
            "removeFile": {
              url: urlBase + "/containers/:container/files/:file",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Container#upload
             * @methodOf lbServices.Container
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `req` – `{object=}` -
             *
             *  - `res` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `result` – `{object=}` -
             */
            "upload": {
              url: urlBase + "/containers/:container/upload",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Container#download
             * @methodOf lbServices.Container
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `container` – `{string=}` -
             *
             *  - `file` – `{string=}` -
             *
             *  - `req` – `{object=}` -
             *
             *  - `res` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "download": {
              url: urlBase + "/containers/:container/download/:file",
              method: "GET",
            },
          }
        );




        /**
        * @ngdoc property
        * @name lbServices.Container#modelName
        * @propertyOf lbServices.Container
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Container`.
        */
        R.modelName = "Container";



        return R;
      }]);


  module
  .factory('LoopBackAuth', function() {
    var props = ['accessTokenId', 'currentUserId', 'rememberMe'];
    var propsPrefix = '$LoopBack$';

    function LoopBackAuth() {
      var self = this;
      props.forEach(function(name) {
        self[name] = load(name);
      });
      this.currentUserData = null;
    }

    LoopBackAuth.prototype.save = function() {
      var self = this;
      var storage = this.rememberMe ? localStorage : sessionStorage;
      props.forEach(function(name) {
        save(storage, name, self[name]);
      });
    };

    LoopBackAuth.prototype.setUser = function(accessTokenId, userId, userData) {
      this.accessTokenId = accessTokenId;
      this.currentUserId = userId;
      this.currentUserData = userData;
    };

    LoopBackAuth.prototype.clearUser = function() {
      this.accessTokenId = null;
      this.currentUserId = null;
      this.currentUserData = null;
    };

    LoopBackAuth.prototype.clearStorage = function() {
      props.forEach(function(name) {
        save(sessionStorage, name, null);
        save(localStorage, name, null);
      });
    };

    return new LoopBackAuth();

    // Note: LocalStorage converts the value to string
    // We are using empty string as a marker for null/undefined values.
    function save(storage, name, value) {
      try {
        var key = propsPrefix + name;
        if (value == null) value = '';
        storage[key] = value;
      } catch (err) {
        console.log('Cannot access local/session storage:', err);
      }
    }

    function load(name) {
      var key = propsPrefix + name;
      return localStorage[key] || sessionStorage[key] || null;
    }
  })
  .config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('LoopBackAuthRequestInterceptor');
  }])
  .factory('LoopBackAuthRequestInterceptor', ['$q', 'LoopBackAuth',
    function($q, LoopBackAuth) {
      return {
        'request': function(config) {
          // filter out external requests
          var host = getHost(config.url);
          if (host && host !== urlBaseHost) {
            return config;
          }

          if (LoopBackAuth.accessTokenId) {
            config.headers[authHeader] = LoopBackAuth.accessTokenId;
          } else if (config.__isGetCurrentUser__) {
            // Return a stub 401 error for User.getCurrent() when
            // there is no user logged in
            var res = {
              body: { error: { status: 401 }},
              status: 401,
              config: config,
              headers: function() { return undefined; },
            };
            return $q.reject(res);
          }
          return config || $q.when(config);
        },
      };
    }])

  /**
   * @ngdoc object
   * @name lbServices.LoopBackResourceProvider
   * @header lbServices.LoopBackResourceProvider
   * @description
   * Use `LoopBackResourceProvider` to change the global configuration
   * settings used by all models. Note that the provider is available
   * to Configuration Blocks only, see
   * {@link https://docs.angularjs.org/guide/module#module-loading-dependencies Module Loading & Dependencies}
   * for more details.
   *
   * ## Example
   *
   * ```js
   * angular.module('app')
   *  .config(function(LoopBackResourceProvider) {
   *     LoopBackResourceProvider.setAuthHeader('X-Access-Token');
   *  });
   * ```
   */
  .provider('LoopBackResource', function LoopBackResourceProvider() {
    /**
     * @ngdoc method
     * @name lbServices.LoopBackResourceProvider#setAuthHeader
     * @methodOf lbServices.LoopBackResourceProvider
     * @param {string} header The header name to use, e.g. `X-Access-Token`
     * @description
     * Configure the REST transport to use a different header for sending
     * the authentication token. It is sent in the `Authorization` header
     * by default.
     */
    this.setAuthHeader = function(header) {
      authHeader = header;
    };

    /**
     * @ngdoc method
     * @name lbServices.LoopBackResourceProvider#getAuthHeader
     * @methodOf lbServices.LoopBackResourceProvider
     * @description
     * Get the header name that is used for sending the authentication token.
     */
    this.getAuthHeader = function() {
      return authHeader;
    };

    /**
     * @ngdoc method
     * @name lbServices.LoopBackResourceProvider#setUrlBase
     * @methodOf lbServices.LoopBackResourceProvider
     * @param {string} url The URL to use, e.g. `/api` or `//example.com/api`.
     * @description
     * Change the URL of the REST API server. By default, the URL provided
     * to the code generator (`lb-ng` or `grunt-loopback-sdk-angular`) is used.
     */
    this.setUrlBase = function(url) {
      urlBase = url;
      urlBaseHost = getHost(urlBase) || location.host;
    };

    /**
     * @ngdoc method
     * @name lbServices.LoopBackResourceProvider#getUrlBase
     * @methodOf lbServices.LoopBackResourceProvider
     * @description
     * Get the URL of the REST API server. The URL provided
     * to the code generator (`lb-ng` or `grunt-loopback-sdk-angular`) is used.
     */
    this.getUrlBase = function() {
      return urlBase;
    };

    this.$get = ['$resource', function($resource) {
      var LoopBackResource = function(url, params, actions) {
        var resource = $resource(url, params, actions);

        // Angular always calls POST on $save()
        // This hack is based on
        // http://kirkbushell.me/angular-js-using-ng-resource-in-a-more-restful-manner/
        resource.prototype.$save = function(success, error) {
          // Fortunately, LoopBack provides a convenient `upsert` method
          // that exactly fits our needs.
          var result = resource.upsert.call(this, {}, this, success, error);
          return result.$promise || result;
        };
        return resource;
      };

      LoopBackResource.getUrlBase = function() {
        return urlBase;
      };

      LoopBackResource.getAuthHeader = function() {
        return authHeader;
      };

      return LoopBackResource;
    }];
  });
})(window, window.angular);

'use strict';

/**
 * @ngdoc function
 * @name travelsApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the travelsApp
 */
angular.module('travelsApp')
  .controller('modalTravelCtrl', function ($uibModalInstance, Travel, currentUser, Employee, travelSelected,toaster) {
    var $ctrl = this;

    $ctrl.travel = {};
    $ctrl.travel.employeeID = currentUser.profile.employeeID;
    $ctrl.sending = false;
    $ctrl.travel.date_begin = new Date();
    $ctrl.travel.date_end = new Date();
    var maxDate = new Date();
    var duration = 15;
    $ctrl.edit = false;

    //We are evaluating if we receive some travel like parameter
    if (travelSelected) {
      $ctrl.travel = travelSelected;
      $ctrl.travel.date_begin = new Date(travelSelected.date_begin);
      $ctrl.travel.date_end = new Date(travelSelected.date_end);
      $ctrl.edit = true;
    }

    console.log(travelSelected);
    maxDate.setTime(maxDate.getTime() + (15 * 24 * 60 * 60 * 1000));

    //configuration for date picker
    $ctrl.dateOptions = {
      // dateDisabled: disabled,
      formatYear: 'yy',
      maxDate: maxDate,
      minDate: new Date(),
      startingDay: 1
    };

    //handler to restrict the pickers to avoid error
    $ctrl.changeDate2 = function () {
      console.log($ctrl.travel.date_end);
      var start = moment($ctrl.travel.date_begin);
      var end = moment($ctrl.travel.date_end);
      if (start > end) {
        console.log("is after");
        $ctrl.travel.date_begin = $ctrl.travel.date_end;
      }
      //
    };

    $ctrl.open1 = function () {
      $ctrl.popup1.opened = true;
    };

    $ctrl.open2 = function () {
      $ctrl.popup2.opened = true;
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);

    $ctrl.popup2 = {
      opened: false
    };
    $ctrl.popup1 = {
      opened: false
    };


    $ctrl.ok = function () {
      //$uibModalInstance.close($ctrl.selected.item);
    };

    $ctrl.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };

    /**
     * @desc function for create or update the travel
     * @param url
     */
    $ctrl.createTravel = function () {
      $ctrl.sending = true;
      console.log($ctrl.travel);
      $ctrl.travel.date_updated = new Date();
      if (!$ctrl.edit) {
        $ctrl.travel.date_created = new Date();
        console.log("create");
        Employee.travels.create({id: currentUser.profile.employeeID}, $ctrl.travel)
          .$promise
          .then(function (res) {
            toaster.pop("success","Request created correctly");
            console.log(res);
            $uibModalInstance.close(res);
            $ctrl.sending = false;
          })
          .catch(function (err) {
            toaster.pop("error","Error saving please try again");
            console.log(err);
            $ctrl.sending = false;
          })
      }
      else {
        console.log("edit");

        Employee.travels.updateById({
            id: currentUser.profile.employeeID,
            fk: $ctrl.travel.id
          },
          $ctrl.travel)
          .$promise
          .then(function (res) {
            toaster.pop("success","Request updated");
            console.log(res);
            $uibModalInstance.close(res);
            $ctrl.sending = false;
          })
          .catch(function (err) {
            toaster.pop("error","Error saving please try again");

            console.log(err);
            $ctrl.sending = false;
          })
      }


    }

  })
  .controller('DashboardCtrl',
    function (Travel, $uibModal, $log, $document, $scope, currentUser,
              Employee, Account, $state, $rootScope) {

      var vm = this;

      console.log("dashboard");

      vm.availablePages = [];
      vm.sort = [];

      //object to setup the pagination
      vm.pagination = {
        pageSize: 10,
        pageNumber: 1,
        totalItems: null,
        getTotalPages: function () {
          vm.availablePages = [];
          let totalPages = Math.ceil(this.totalItems / this.pageSize);
          for (let i = 0; i < totalPages; i++) {
            vm.availablePages.push(i + 1);
          }

          return totalPages;

        },
        nextPage: function () {
          if (this.pageNumber < this.getTotalPages()) {
            this.pageNumber++;
            vm.load();
          }
        },
        previousPage: function () {
          if (this.pageNumber > 1) {
            this.pageNumber--;
            vm.load();
          }
        }
      };
      /**
       * @desc function for load the last activity
       * @param url
       */
      vm.loadLastActivity = () => {
        Employee.travels({
        id: currentUser.profile.employeeID,
        filter: {
          order: ['date_updated desc'],
          limit: 1
        }
      })
        .$promise
        .then(function (resTravels) {

          vm.last_travel = resTravels[0];
          Employee.expenses({
            id: currentUser.profile.employeeID,
            filter: {
              order: ['date_updated desc'],
              limit: 1
            }
          })
            .$promise
            .then((resLasExp) => {
              vm.last_expense = resLasExp[0];
              console.log([vm.last_expense, vm.last_travel]);
              var updateExpense = moment(vm.last_expense.date_updated);
              var updateTravel = moment(vm.last_travel.date_updated);
              console.log([updateExpense , updateTravel])
              if (updateTravel < updateExpense) {
                vm.activityTravel = false;
              }else{
                vm.activityTravel = true;
              }
            })
            .catch((errExp) => {
              console.log(errExp)
            })

        })
        .catch(function (err) {
          console.log(err);

        })
      }

      /**
       * @desc function for load the data depending filters
       * @param url
       */
      vm.load = function () {
        console.log([vm.pagination.pageSize, vm.pagination.pageNumber, vm.sort, vm.where]);
        vm.filter = {};
        vm.filter.where = vm.where;
        vm.filter.limit = vm.pagination.pageSize;
        vm.filter.skip = (vm.pagination.pageNumber - 1) * vm.pagination.pageSize;
        vm.filter.order = vm.sort;
        console.log(vm.filter);


        Employee.travels({
          id: currentUser.profile.employeeID,
          filter: vm.filter
        })
          .$promise
          .then(function (resTravels) {
            Employee.travels.count({
              id: currentUser.profile.employeeID
            })
              .$promise
              .then(function (resCount) {
                console.log(resCount);
                console.log(resTravels);
                vm.gridOptions.data = resTravels;
                vm.pagination.totalItems = resCount.count;
                vm.loadLastActivity();
              })
              .catch(function (err) {

              });


          })
          .catch(function (err) {
            console.log(err);
          });

      };

      //object to setup the grid
      vm.gridOptions = {
        excludeProperties: '__metadata',
        enablePaginationControls: false,
        useExternalSorting: true,
        useExternalFiltering: true,
        enableFiltering: true,
        columnDefs: [

          {name: 'destination', width: 130},
          {
            name: 'date_begin',
            width: 110,
            enableFiltering: false,
            displayName: 'Begins',
            cellTemplate: '<p class="margint-top-5">{{ grid.appScope.formatDate(COL_FIELD)}}</p>'
          },
          {
            name: 'date_end',
            width: 110,
            enableFiltering: false,
            displayName: 'Ends',
            cellTemplate: '<p class="margint-top-5">{{ grid.appScope.formatDate(COL_FIELD)}}</p>'
          },
          {
            name: 'date_created',
            width: 110,
            enableFiltering: false,
            displayName: 'Created',
            cellTemplate: '<p class="margint-top-5">{{ grid.appScope.formatDate(COL_FIELD)}}</p>'
          },
          {
            name: 'required_ammount', width: 140, displayName: 'Req. ammount',
            cellTemplate: '<p class="margint-top-5">{{ grid.appScope.formatWithCurrency(COL_FIELD)}}</p>'
          },
          {name: 'project', width: 90},
          {name: 'customer', width: 220},
          {
            name: 'id',
            width: 90,
            displayName: 'Actions',
            enableFiltering: false,
            cellTemplate: '<div style="text-align: center;margin-bottom: 15px;">' +
            '<button style="height: 25px !important;vertical-align: text-top;margin-right:5px !important;" class="btn btn-primary btn-sm" id="fila" ' +
            'ng-click="grid.appScope.goto(\'{{COL_FIELD }}\' )">' +
            '<i class="fa fa-eye"></i></button>' +
            '<button style="height: 25px !important;vertical-align: text-top;" class="btn btn-success btn-sm" id="fila" ' +
            'ng-click="grid.appScope.edit( row.entity)">' +
            '<i class="fa fa-pencil"></i></button>' +
            '</div>'
          },
        ],
        onRegisterApi: function (gridApi) {
          vm.gridApi = gridApi;
          //declare the events

          vm.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
            vm.sort = [];
            angular.forEach(sortColumns, function (sortColumn) {
              //  vm.sort.push({fieldName: sortColumn.name, order: sortColumn.sort.direction});
              vm.sort.push(sortColumn.name + " " + sortColumn.sort.direction);
            });


            vm.load();
          });

          vm.gridApi.core.on.filterChanged($scope, function () {
            //vm.filter = [];
            vm.where = {};
            var grid = this.grid;
            angular.forEach(grid.columns, function (column) {
              var fieldName = column.field;
              var value = column.filters[0].term;
              var operator = "like";
              if (value) {
                if (fieldName == "id") operator = "equals";
                else if (fieldName == "required_ammount") operator = "gte";
                /*vm.filter.push({
                 fieldName: fieldName,
                 operator: operator,
                 value: value
                 });*/
                vm.where[fieldName] = {
                  [operator]: value
                };
              }
            });

            vm.load();
          });
        }
      };

      /**
       * @desc function for setup the data of the view
       * @param url
       */
      vm.init = function () {
        Account.employee({id: currentUser.profile.clientID})
          .$promise
          .then(function (res) {
            console.log(res);
            vm.employee = res;
            currentUser.setProfile(res.id, currentUser.profile.token, null, null, currentUser.profile.clientID);
            vm.load();

          })
          .catch(function (err) {
            console.log(err);
          });



      }


      /**
       * @desc function for change location depending of url
       * @param url
       */
      $scope.goto = function (url) {
        console.log("goto");
        $state.go('travels', {travelID: url});
        // $location.path(parser.pathname).search(params);
      };
      /**
       * @desc function for change the ammount depending the currency
       * @param url
       */
      $scope.formatWithCurrency = function (ammount) {


        if (currentUser.currency.selectedCurrency !== "MXN") {
          return parseFloat(ammount * $rootScope.dataRates.rates[currentUser.currency.selectedCurrency]).toFixed(2) +
            " " + currentUser.currency.selectedCurrency;
        } else {
          return ammount + " MXN";
        }
      };
      /**
       * @desc function for change the ammount depending the currency
       * @param value
       */
      vm.formatWithCurrency = (value) => {
        return $scope.formatWithCurrency(value);
      };

      $scope.edit = function (travelSelected) {

        vm.open(travelSelected);

      };
      $scope.formatDate = (date) => {
        return moment(date).format("DD-MM-YY HH:mm");
      };
      /**
       * @desc function for return date formatted from view
       * @param value
       */
      vm.formatDate = (value) => {
        return $scope.formatDate(value);
      };


      vm.animationsEnabled = true;

      /**
       * @desc function for open the modal of the travel
       * @param url
       */
      vm.open = function (travelSelected) {
        var parentElem = undefined;
        var modalInstance = $uibModal.open({
          animation: vm.animationsEnabled,
          ariaLabelledBy: 'modal-title',
          ariaDescribedBy: 'modal-body',
          templateUrl: '../views/modals/travelModal.html',
          controller: 'modalTravelCtrl',
          controllerAs: '$ctrl',
          size: "md",
          appendTo: parentElem,
          resolve: {
            travelSelected: function () {
              return travelSelected ? travelSelected : null;
            }
          }
        });

        modalInstance.result.then(function (resModal) {
          vm.load();

        }, function () {
          console.log('Modal dismissed at: ' + new Date());
        });
      };

      vm.init();

    });

'use strict';

/**
 * @ngdoc function
 * @name travelsApp.controller:TravelsCtrl
 * @description
 * # TravelsCtrl
 * Controller of the travelsApp
 */
angular.module('travelsApp')
  .controller('ExpenseModalCtrl',
    function ($uibModalInstance, Travel, currentUser,
              Employee, expenseSelected, travelID, maxBudget, SweetAlert, toaster, Upload, $scope, Container) {
      var $ctrl = this;


      $ctrl.expense = {};
      $ctrl.expense.travelID = travelID;
      $ctrl.sending = false;
      $ctrl.expense.date = new Date();

      var maxDate = new Date();
      var duration = 15;
      $ctrl.edit = false;
      if (expenseSelected) {
        console.log(expenseSelected);
        $ctrl.expense = expenseSelected;
        console.log($ctrl.file)
        $ctrl.image = APIURL+"/containers/"+$ctrl.expense.id+"/download/"+$ctrl.expense.file_name;
        $ctrl.expense.date = new Date(expenseSelected.date);
        $ctrl.edit = true;
      }

      console.log(maxBudget);
      maxDate.setTime(maxDate.getTime() + (15 * 24 * 60 * 60 * 1000));

      $ctrl.validateAndSendFile = function () {
        if ($ctrl.file) {

          $ctrl.upload($ctrl.file, $ctrl.container);
        }
      };
      $scope.$watch(() => {
        return $ctrl.file;
      }, (newVal, oldVal, another) => {
        console.log($ctrl.file);

      })
      // upload on file select or drop
      $ctrl.upload = function (file, container) {
        Upload.upload({
          url: APIURL + "/containers/" + container + "/upload",
          data: {file: file}
        }).then(function (resp) {
          toaster.pop("success", "Image saved");
          console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ');
          console.log(resp.data)
        }, function (err) {
          console.log('Error status: ');
          console.log(err);
        }, function (evt) {
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });
      };

      $ctrl.dateOptions = {
        // dateDisabled: disabled,
        formatYear: 'yy',
        maxDate: maxDate,
        minDate: new Date(),
        startingDay: 1
      };


      $ctrl.open1 = function () {
        $ctrl.popup1.opened = true;
      };


      var tomorrow = new Date();
      tomorrow.setDate(tomorrow.getDate() + 1);
      var afterTomorrow = new Date();
      afterTomorrow.setDate(tomorrow.getDate() + 1);


      $ctrl.popup1 = {
        opened: false
      };


      $ctrl.ok = function () {
        //$uibModalInstance.close($ctrl.selected.item);
      };

      /**
       * @desc function for cancel the modal
       * @param url
       */
      $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };

      /**
       * @desc function for save or update the expense
       * @param url
       */
      $ctrl.saveExpense = function () {
        $ctrl.sending = true;
        console.log($ctrl.expense);
        $ctrl.expense.date_updated = new Date();
        $ctrl.expense.travel_id = travelID;

        Travel.totalOfExpenses({
          idTravel: travelID
        })
          .$promise
          .then((resAmmount) => {
            console.log(resAmmount)
            var total = resAmmount.ammount + $ctrl.expense.ammount;
            console.log(total);
            if (total > maxBudget) {
              SweetAlert.swal({
                  title: "Warning!",
                  text: "You are exceding the maximum budget covered by the company , are you sure?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55", confirmButtonText: "Yes",
                  cancelButtonText: "No!",
                  closeOnConfirm: true,
                  closeOnCancel: true
                },
                function (isConfirm) {
                  if (isConfirm) {
                    $ctrl.saveOrEditExpense();
                  } else {
                    $uibModalInstance.dismiss('cancel');
                  }
                });
            } else {
              $ctrl.saveOrEditExpense();
            }

          })
          .catch((err) => {
            console.log(err)
          })


      }
      $ctrl.createContainerAndSave = (idExpense) => {
        Container.createContainer({
          name: idExpense
        })
          .$promise
          .then((resCont) => {
            console.log(resCont);
            $ctrl.container = resCont.name;
            $ctrl.validateAndSendFile();
          })
          .catch((errCont) => {
            toaster.pop("error", "Error saving image please try again");
          })
      };
      $ctrl.searchContainerAndSave = (idExpense)=>{
        Container.getContainer({
          container: idExpense
        })
          .$promise
          .then((resCont)=>{
            console.log(resCont);
            $ctrl.container = resCont.name;
            $ctrl.validateAndSendFile();
          })
          .catch((err)=>{
              console.log(err)
          })
      }

      $ctrl.saveOrEditExpense = function () {
        $ctrl.expense.file_name = $ctrl.file.name;
        if (!$ctrl.edit) {
          console.log("create");
          Employee.expenses.create({
            id: currentUser.profile.employeeID
          }, $ctrl.expense)
            .$promise
            .then(function (resExp) {
              console.log(resExp);
              toaster.pop("success", "Request created");

              $uibModalInstance.close(resExp);
              $ctrl.sending = false;

              $ctrl.createContainerAndSave(resExp.id);
            })
            .catch(function (err) {
              toaster.pop("error", "Error saving please try again");

              console.log(err);

            });
        }
        else {
          console.log("edit");

          Employee.expenses.updateById({
            id: currentUser.profile.employeeID,
            fk: $ctrl.expense.id
          }, $ctrl.expense)
            .$promise
            .then(function (resExp) {
              console.log(resExp);
              toaster.pop("success", "Request updated");
              $ctrl.searchContainerAndSave(resExp.id);
              $uibModalInstance.close(resExp);
              $ctrl.sending = false;
            })
            .catch(function (err) {
              toaster.pop("error", "Error saving please try again");

              console.log(err);
            });
        }
      }

    })
  .controller('TravelsCtrl',
    function ($rootScope, $stateParams, Travel, Expense, Employee,
              currentUser, $uibModal, currencyConversion, $scope, $state) {

      var vm = this;
      vm.animationsEnabled = true;
      vm.availablePages = [];
      vm.sort = [];

      console.log($stateParams.travelID);

      vm.isEdit = false;

      if ($stateParams.travelID) {
        vm.isEdit = true;
        vm.travelID = $stateParams.travelID;
      }
      /**
       * @desc function for change the ammount depending the currency
       * @param url
       */
      $scope.formatWithCurrency = function (ammount) {


        if (currentUser.currency.selectedCurrency !== "MXN") {
          return parseFloat(ammount * $rootScope.dataRates.rates[currentUser.currency.selectedCurrency]).toFixed(2) +
            " " + currentUser.currency.selectedCurrency;
        } else {
          return ammount + " MXN";
        }
      };
      /**
       * @desc function for initialize the view
       * @param url
       */
      vm.init = function () {

        if (vm.isEdit) {
          Employee.travels.findById({
            id: currentUser.profile.employeeID,
            fk: vm.travelID
          })
            .$promise
            .then(function (resTravel) {
              vm.travel = resTravel;
              console.log(resTravel);
              vm.load();
            })
            .catch(function (err) {
              console.log(err);
            })
        }
        else {


          Employee.travels({
            id: currentUser.profile.employeeID,
            filter: {
              order: ['date_created desc'],
              limit: 1
            }
          })
            .$promise
            .then(function (resTravel) {
              console.log(resTravel);
              vm.travel = resTravel[0];
              vm.load();
            })
            .catch(function (err) {
              console.log(err);
            });
        }
      }

      /**
       * @desc function for open the modal of the expenses
       * @param url
       */
      vm.openExpense = function (expense) {
        var expenseToOpen = {};
        expenseToOpen = _.clone(expense);
        var parentElem = undefined;
        var modalInstance = $uibModal.open({
          animation: vm.animationsEnabled,
          ariaLabelledBy: 'modal-title',
          ariaDescribedBy: 'modal-body',
          templateUrl: '../views/modals/expenseModal.html',
          controller: 'ExpenseModalCtrl',
          controllerAs: '$ctrl',
          size: "md",
          appendTo: parentElem,
          resolve: {
            expenseSelected: function () {
              return expense ? expenseToOpen : null;
            },
            travelID: function () {
              return vm.travel.id;

            },
            maxBudget: function () {
              return vm.travel.max_budget;
            }
          }
        });

        modalInstance.result.then(function (resModal) {
          vm.load();
        }, function () {
          console.log('Modal dismissed at: ' + new Date());
        });
      };

      //Object to setup the pagination of ui- grid
      vm.pagination = {
        pageSize: 10,
        pageNumber: 1,
        totalItems: null,
        getTotalPages: function () {
          vm.availablePages = [];
          let totalPages = Math.ceil(this.totalItems / this.pageSize);
          for (let i = 0; i < totalPages; i++) {
            vm.availablePages.push(i + 1);
          }

          return totalPages;

        },
        nextPage: function () {
          if (this.pageNumber < this.getTotalPages()) {
            this.pageNumber++;
            vm.load();
          }
        },
        previousPage: function () {
          if (this.pageNumber > 1) {
            this.pageNumber--;
            vm.load();
          }
        }
      };

      /**
       * @desc function for load the records depending on filters
       * @param
       */
      vm.load = function () {
        console.log([vm.pagination.pageSize, vm.pagination.pageNumber, vm.sort, vm.where]);
        vm.filter = {};
        vm.filter.where = vm.where;
        vm.filter.limit = vm.pagination.pageSize;
        vm.filter.skip = (vm.pagination.pageNumber - 1) * vm.pagination.pageSize;
        vm.filter.order = vm.sort;
        console.log(vm.filter);


        Travel.expenses({
          id: vm.travel.id,
          filter: vm.filter
        })
          .$promise
          .then(function (resExpenses) {
            Travel.expenses.count({
              id: vm.travel.id
            })
              .$promise
              .then(function (resCount) {
                console.log(resCount);
                console.log(resExpenses);
                vm.gridOptions.data = resExpenses;
                vm.pagination.totalItems = resCount.count;
              })
              .catch(function (err) {

              });


          })
          .catch(function (err) {
            console.log(err);
          });

      };

      //OBject to setup the options of ui-grid
      vm.gridOptions = {
        excludeProperties: '__metadata',
        enablePaginationControls: false,
        useExternalSorting: true,
        useExternalFiltering: true,
        enableFiltering: true,
        columnDefs: [

          {name: 'type', width: 130},
          {
            name: 'date',
            width: 140,
            enableFiltering: false,
            displayName: 'Date created',
            cellTemplate: '<p class="margint-top-5">{{ grid.appScope.formatDate(COL_FIELD)}}</p>'
          },

          {
            name: 'ammount', width: 140,
            cellTemplate: '<p class="margint-top-5">{{ grid.appScope.formatWithCurrency(COL_FIELD)}}</p>'
          },
          {
            name: 'id',
            width: 90,
            displayName: 'Actions',
            enableFiltering: false,
            cellTemplate: '<div style="text-align: center;margin-bottom: 15px;">' +
            '<button style="height: 25px !important;vertical-align: text-top;margin-right:5px !important;" class="btn btn-primary btn-sm" id="fila" ' +
            'ng-click="grid.appScope.goto(\'{{COL_FIELD }}\' )">' +
            '<i class="fa fa-eye"></i></button>' +
            '<button style="height: 25px !important;vertical-align: text-top;" class="btn btn-success btn-sm" id="fila" ' +
            'ng-click="grid.appScope.edit( row.entity)">' +
            '<i class="fa fa-pencil"></i></button>' +
            '</div>'
          },
        ],
        onRegisterApi: function (gridApi) {
          vm.gridApi = gridApi;
          //declare the events

          vm.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
            vm.sort = [];
            angular.forEach(sortColumns, function (sortColumn) {
              //  vm.sort.push({fieldName: sortColumn.name, order: sortColumn.sort.direction});
              vm.sort.push(sortColumn.name + " " + sortColumn.sort.direction);
            });


            vm.load();
          });

          vm.gridApi.core.on.filterChanged($scope, function () {
            //vm.filter = [];
            vm.where = {};
            var grid = this.grid;
            angular.forEach(grid.columns, function (column) {
              var fieldName = column.field;
              var value = column.filters[0].term;
              var operator = "like";
              if (value) {
                if (fieldName == "id") operator = "equals";
                else if (fieldName == "ammount") operator = "gte";
                /*vm.filter.push({
                 fieldName: fieldName,
                 operator: operator,
                 value: value
                 });*/
                vm.where[fieldName] = {
                  [operator]: value
                };
              }
            });

            vm.load();
          });
        }
      };
      /**
       * @desc function for change location depending of url
       * @param url
       */
      $scope.goto = (url) => {
        console.log("goto");
        $state.go('expenses', {expenseID: url, travelID: vm.travel.id});
        // $location.path(parser.pathname).search(params);
      };

      /**
       * @desc function for format the dates from ui grid
       * @param date
       */
      $scope.formatDate = (date) => {
        return vm.formatDate(date);
      };
      /**
       * @desc function for format the dates from actual scope
       * @param url
       */
      vm.formatDate = (date) => {
        return moment(date).format("DD-MM-YY HH:mm");
      };
      /**
       * @desc function for open the modals of the travel
       * @param expense
       */
      $scope.edit = (expenseSelected) => {
        vm.openExpense(expenseSelected);
      };

      vm.openTravel = (travelSelected) => {
        var travelToOpen = {};
        travelToOpen = _.clone(travelSelected);
        var parentElem = undefined;
        var modalInstance = $uibModal.open({
          animation: vm.animationsEnabled,
          ariaLabelledBy: 'modal-title',
          ariaDescribedBy: 'modal-body',
          templateUrl: '../views/modals/travelModal.html',
          controller: 'modalTravelCtrl',
          controllerAs: '$ctrl',
          size: "md",
          appendTo: parentElem,
          resolve: {
            travelSelected: function () {
              return travelSelected ? travelToOpen : null;
            }
          }
        });

        modalInstance.result.then(function (resModal) {
          vm.travel = resModal;
          vm.load();
        }, function () {
          console.log('Modal dismissed at: ' + new Date());
        });
      };
      vm.init();


    });

'use strict';

/**
 * @ngdoc function
 * @name travelsApp.controller:ExpensesCtrl
 * @description
 * # ExpensesCtrl
 * Controller of the travelsApp
 */
angular.module('travelsApp')
  .controller('ExpensesCtrl', function ($rootScope, $stateParams, Travel, Expense,
                                        currentUser, $uibModal, currencyConversion, $scope, $state,Container) {

    var vm = this;
    //we will evaluate if we are receiving the expense id
    if ($stateParams.expenseID) {
      vm.isEdit = true;
      vm.expenseID = $stateParams.expenseID;
      vm.travelID = $stateParams.travelID;
      vm.travel = undefined;
    }
    /**
     * @desc function to format dates
     * @param url
     */
    vm.formatDate = (date) => {
      return moment(date).format("DD-MM-YY HH:mm");
    };

    /**
     * @desc function for initialize the data of the view
     * @param url
     */
    vm.init = () => {
      if (vm.isEdit) {
        Expense.findById({
          id: vm.expenseID,

        })
          .$promise
          .then(function (resExp) {
            vm.expense = resExp;
            console.log(resExp);
            Travel.findById({
              id: vm.travelID
            })
              .$promise
              .then((resTravel) => {
                console.log(resTravel)
                vm.travel = resTravel;
                vm.image = APIURL+"/containers/"+vm.expense.id+"/download/"+vm.expense.file_name;

              })
              .catch((errTravel) => {

              })
          })
          .catch(function (err) {
            console.log(err);
          })
      }

    };
    /**
     * @desc function for open the modal of the expenses
     * @param url
     */
    vm.openExpense = function (expense) {
      var expenseToOpen = {};
      expenseToOpen = _.clone(expense);
      var parentElem = undefined;
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: '../views/modals/expenseModal.html',
        controller: 'ExpenseModalCtrl',
        controllerAs: '$ctrl',
        size: "md",
        appendTo: parentElem,
        resolve: {
          expenseSelected: function () {
            return expense ? expenseToOpen : null;
          },
          travelID: function () {
            return vm.travelID;

          },
          maxBudget: function () {
            return vm.travel.max_budget;
          }
        }
      });

      modalInstance.result.then(function (resModal) {
        vm.expense = resModal;
        vm.image = APIURL+"/containers/"+vm.expense.id+"/download/"+vm.expense.file_name;
      }, function () {
        console.log('Modal dismissed at: ' + new Date());
      });
    };
    /**
     * @desc function for change the ammount depending the currency
     * @param url
     */
    $scope.formatWithCurrency = function (ammount) {


      if (currentUser.currency.selectedCurrency !== "MXN") {
        return parseFloat(ammount * $rootScope.dataRates.rates[currentUser.currency.selectedCurrency]).toFixed(2) +
          " " + currentUser.currency.selectedCurrency;
      } else {
        return ammount + " MXN";
      }
    };
    vm.formatWithCurrency = (value) => {
      return $scope.formatWithCurrency(value);
    };
    //Execute the call to load the basic data of the view
    vm.init();
  });

'use strict';

/**
 * @ngdoc directive
 * @name directive:numbersonly
 * @description
 * # numbersonly
 */
angular.module('travelsApp')
    .directive('numbersOnly', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attr, ngModelCtrl) {
                function fromUser(text) {
                    if (text) {
                        var transformedInput = text.replace(/[^0-9]/g, '');

                        if (transformedInput !== text) {
                            ngModelCtrl.$setViewValue(transformedInput);
                            ngModelCtrl.$render();
                        }
                        return transformedInput;
                    }
                    return undefined;
                }
                ngModelCtrl.$parsers.push(fromUser);
            }
        };
    });

'use strict';

/**
 * @ngdoc service
 * @name travelsApp.currencyConversion
 * @description
 * # currencyConversion
 * Factory in the travelsApp.
 */
angular.module('travelsApp')
  .factory('currencyConversion', function ($http) {
    // Service logic
    // ...
    var newAmmount = 0;

    function  handleSuccess(response) {
        //console.log(response);

        if (typeof response.data !== 'undefined') {
            return ( response.data );
        } else {
            return ( response );
        }
    };

    var getCurrencies=  function(currencyIndex,ammount){
       return $http.get('https://api.fixer.io/latest?base='+currencyIndex)
        .then(handleSuccess);
    }

    // Public API here
    return {
      getCurrencies: getCurrencies
    };
  });

'use strict';

/**
 * @ngdoc function
 * @name travelsApp.controller:NavbarCtrl
 * @description
 * # NavbarCtrl
 * Controller of the travelsApp
 */
angular.module('travelsApp')
  .controller('NavbarCtrl', function ($rootScope, $scope, currentUser) {

    var vm =this;

    vm.onChangeCurrency = () => {
      console.log("Change currency...");
    }

    $scope.$watch(()=>{
        return $rootScope.selectedCurrency;
    },(newVal, oldVal, another)=>{
        console.log($rootScope.selectedCurrency);

        if($rootScope.selectedCurrency!==undefined){
          currentUser.setCurrency($rootScope.selectedCurrency);

        }
    })



  });
