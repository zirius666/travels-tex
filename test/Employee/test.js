/*describe('/Accounts test suite', function() {

    describe('login/logout for Test  account', function() {
        var token;
        var verificationToken;
        var demoUserId;
        var redirectLink;

        it('should NOT be able to create a accounts with fake email', function(done) {
            api.post('/api/accounts')
                .send({ email: "new..@example.com", password: "mypassword" })
                .expect(422, done);
        });

        it('should be able to create new account ', function(done) {
            api.post('/api/accounts')
                .send({ email: "new132@example.com", password: "mypassword" })
                .expect(200, done);
        });
        
        it('should login as Test  account', function(done) {
            api.post('/api/accounts/login')
                .send({ email: "new132@example.com", password: "myadminpassword" })
                .expect(200)
                .end(function(err, res) {
                    if (err) return done(err);
                    token = res.body.id;
                    done();
                });
        });

        it('should be able to create a Demo user', function(done) {

            api.post('/api/accounts')
                .query({ access_token: token })
                .send({ email: "de...@example.com", password: "mydemopassword" })
                .expect(200)
                .end(function(err, res) {
                    if (err) return done(err);
                    verificationToken = res.body.verificationToken;
                    demoUserId = res.body.id;
                    redirectLink = 'http://0.0.0.0/verified?user_id=' + demoUserId;
                    done();
                });
        });

        it('should logout from Test Admin user', function(done) {
            api.post('/api/Users/logout')
                .query({ access_token: token })
                .expect(204, done);
        });

        it('should be able to verify the Demo user', function(done) {

            api.get('/api/Users/confirm')
                .query({ uid: demoUserId, redirect: redirectLink, token: verificationToken })
                .expect(302, done);
        });

    });

    describe('login/logout for regular User', function() {
        // ...
    });

    describe('restrictions for non-Admin users', function() {
        //        ...
    });

});
*/