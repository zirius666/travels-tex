var assert = require('assert');
describe('/Travels test suite', function () {

  describe('Testing for expensesTotal remote method', function () {
    var token;
    var verificationToken;
    var accountID;
    var redirectLink;
    var employeeAccount;
    var travelID;
    var expenseID;

    it('should be able to login', function (done) {
      api.post('/api/accounts/login')
        .send({email: "a@a.com", password: "admin"})
        .expect(200)
        .end(function (err, res) {

          if (err) return done(err);
          token = res.body.id;
          accountID = res.body.userId;
          done();
        });
    });

    it('should be able to get employeeID', function (done) {
      api.get(`/api/accounts/${accountID}/employee`)
        .query({access_token: token})
        .send({email: "a@a.com", password: "admin"})
        .expect(200)
        .end(function (err, res) {

          if (err) return done(err);
          employeeAccount = res.body.id;
          done();
        });
    });


    it('should be create a travel', function (done) {
      api.post('/api/travels')
        .query({access_token: token})
        .send({
          "destination": "Manhatan",
          "max_budget": 20000,
          "date_created": "2017-08-24T22:10:32.654Z",
          "date_begin": "2017-08-24T22:10:32.654Z",
          "date_end": "2017-08-24T22:10:32.654Z",
          "date_updated": "2017-08-24T22:10:32.654Z",
          "required_ammount": 10000,
          "project": "USAA",
          "customer": "Emmanuel",
          "employee_id": `${employeeAccount}`
        })
        .expect(200)
        .end(function (err, res) {

          if (err) return done(err);
          travelID = res.body.id;
          done();
        });
    });

    it('should be create a expense with "X" ammount ',  function (done) {
      api.post(`/api/employees/${employeeAccount}/expenses`)
        .query({access_token: token})
        .send({
          "ammount": 333,
          "type": "Food",
          "date": "2017-08-24T10:45:55.435Z",
          "date_updated": "2017-08-24T10:45:55.435Z",
          "travel_id": `${travelID}`,
          "employee_id": `${employeeAccount}`
        })
        .expect(200)
        .end(function (err, res) {
          expenseID = res.body.id;
          if (err) return done(err);
          done();
        });
    });

    it('should return the ammount of 333 which is the acummulate at the moment ',  function (done) {
      api.get(`/api/travels/${travelID}/totalExpenses`)
        .query({access_token: token})
        .send()
        .expect(200)
        .end(function (err, res) {
          if (err) return done(err);
          //console.log(res.body)
          assert.equal(res.body.ammount , 333);
          done();
        });
    });

    it('should delete the expense created ',  function (done) {
      api.delete(`/api/expenses/${expenseID}`)
        .query({access_token: token})
        .send()
        .expect(200)
        .end(function (err, res) {
          if (err) return done(err);
          //console.log(res.body)

          done();
        });
    });

    it('should delete the travel created ',  function (done) {
      api.delete(`/api/travels/${travelID}`)
        .query({access_token: token})
        .send()
        .expect(200)
        .end(function (err, res) {
          if (err) return done(err);
          //console.log(res.body)

          done();
        });
    });

  });

});
