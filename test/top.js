var app;
var supertest;
var h;
// Uncomment if you want to include common exports.options or similar
//var common = require("./common");


function importTest(name, path) {
    describe(name, function() {
        require(path);
    });
};


describe("My Test Suite", function() {
    before(function(done) {

        app = require('../server/server');
        supertest = require('supertest');
        h = require('loopback-testing-helpers')(app);

        global.api = supertest(app);

        done();


    });

    beforeEach(function() {
        //console.log("running something before each test");
    });

    importTest("User", './Travel/test');

    after(function() {
        console.log("after all tests");
    });
});
