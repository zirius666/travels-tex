'use strict';

module.exports = function (Travel) {

  var app = require('../../server/server');
  /**
   * Method used to obtain how much is registered on expenses
   * @param {string} id Argument required to findById the travel
   * @param {Function(Error, number)} callback
   */

  Travel.totalOfExpenses = function (idTravel, callback) {

    var ammount = 0;
    Travel.find({
      where :{
        id:idTravel
      },
      include: ['expenses']

    }, function (err ,res) {

      var expensesPromise = res[0].expenses;

      expensesPromise.find()
        .then(function (expenses) {

          ammount = expenses.reduce(function (last, actual) {

             return last + parseFloat(actual.ammount);
          },0)
          callback(null, ammount);
        })

    });

  };

};
